package hu.tmwx;

public class Triangle{
	static final String limitError = "Value of % is not in the range of permitted values.";

	public static String validate(int a, int b, int c, Boolean limit){
		StringBuilder sb = new StringBuilder();
		if(limit){
			if(!(a > 0 && a <= 200)){
				sb.append(limitError.replace('%', 'a')).append("\n");
			}
			if(!(b > 0 && b <= 200)){
				sb.append(limitError.replace('%', 'b')).append("\n");
			}
			if(!(c > 0 && c <= 200)){
				sb.append(limitError.replace('%', 'c')).append("\n");
			}
			if(!sb.isEmpty()){
				return sb.toString();
			}
		}
		if((a < b + c) && (b < a + c) && (c < a + b)){
			if(a == b && a == c){
				return "Equilateral";
			}
			if(a == b || a == c || b == c){
				return "Isosceles";
			}
			return "Scalene";
		}else{
			return "NotATriangle";
		}
	}
}
