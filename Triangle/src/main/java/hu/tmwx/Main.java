package hu.tmwx;




import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ConfigurableApplicationContext;


public class Main extends Application {

    private ConfigurableApplicationContext applicationContext;

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void init(){
        applicationContext = new SpringApplication(EntryApplication.class).run();
    }

    @Override
    public void stop(){
        applicationContext.close();
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        applicationContext.publishEvent(new StageReadyEvent(primaryStage));

    }

    static class StageReadyEvent extends ApplicationEvent {
        public StageReadyEvent(Stage primaryStage) {
            super(primaryStage);
        }

        public Stage getStage() {
            return ((Stage) getSource());
        }
    }

}
