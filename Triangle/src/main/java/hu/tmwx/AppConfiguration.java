package hu.tmwx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppConfiguration  {



	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${application.validate.limit:false}")
	private Boolean doValidate;



    @Bean
    public ApplicationContext applicationContext() {
	    return new ApplicationContext(doValidate);

    }

}