package hu.tmwx;


import org.springframework.core.io.Resource;

import java.security.KeyStore;



public class ApplicationContext {

	Boolean doValidation;

	public ApplicationContext(Boolean doValidation){
		this.doValidation = doValidation;
	}

	public Boolean getDoValidation(){
		return doValidation;
	}
}
