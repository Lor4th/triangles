package hu.tmwx;


import javafx.geometry.Insets;
import javafx.geometry.Pos;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

@Component
public class StageInitializer implements ApplicationListener<Main.StageReadyEvent>{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	Stage window;


	private static final double WINDOW_WIDTH = 600;
	private static final double WINDOW_HEIGHT = 500;

	private static final double WINDOW_WIDTH_LEAVING = 700;
	private static final double WINDOW_HEIGHT_LEAVING = 600;

	private static final double INPUT_WIDTH = 250;
	private static final double LABEL_WIDTH = 200;


	private static final double WINDOW_WIDTH_ARRIVING = 1150;
	private static final double WINDOW_HEIGHT_ARRIVING = 650;
	private final ApplicationContext applicationContext;

	private final TextFormatter<String> textFormatter1;
	private final TextFormatter<String> textFormatter2;
	private final TextFormatter<String> textFormatter3;
	private final TextFormatter<String> textFormatter4;
	private final TextFormatter<String> textFormatter5;
	private final TextFormatter<String> textFormatter6;
	private final TextFormatter<String> textFormatter7;

	List<TextField> fields = new ArrayList<>();

	List<Float> temps;

	public StageInitializer( ApplicationContext applicationContext){
		this.applicationContext = applicationContext;
		UnaryOperator<TextFormatter.Change> filter = change -> {
			String text = change.getControlNewText();
			if (text.matches("[+-]?([0-9]*[.,])?[0-9]*")) {
				return change;
			}
			return null;
		};
		this.textFormatter1= new TextFormatter<>(filter);
		this.textFormatter2= new TextFormatter<>(filter);
		this.textFormatter3= new TextFormatter<>(filter);
		this.textFormatter4= new TextFormatter<>(filter);
		this.textFormatter5= new TextFormatter<>(filter);
		this.textFormatter6= new TextFormatter<>(filter);
		this.textFormatter7= new TextFormatter<>(filter);
	}


	@Override
	public void onApplicationEvent(Main.StageReadyEvent event){
		Stage primaryStage = event.getStage();

		window = primaryStage;
		window.setTitle("Triangle");

		GridPane grid = new GridPane();
		grid.setPadding(new Insets(20, 20, 20, 20));
		grid.setPrefSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		grid.setMinSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		grid.setVgap(8);
		grid.setHgap(80);

		Scene scene = new Scene(grid, WINDOW_WIDTH, WINDOW_HEIGHT);


		Label hetfoLabel = new Label();
		hetfoLabel.setText("hétfő:");

		TextField hetfoInput = new TextField();
		fields.add(hetfoInput);
		hetfoInput.setTextFormatter(textFormatter1);
		hetfoInput.setMaxWidth(INPUT_WIDTH);
		hetfoInput.setMinWidth(INPUT_WIDTH);

		Label keddLabel = new Label();
		keddLabel.setText("kedd:");

		TextField keddInput = new TextField();
		fields.add(keddInput);
		keddInput.setTextFormatter(textFormatter2);
		keddInput.setMaxWidth(INPUT_WIDTH);
		keddInput.setMinWidth(INPUT_WIDTH);

		Label szerdaLabel = new Label();
		szerdaLabel.setText("szerda:");

		TextField szerdaInput = new TextField();
		fields.add(szerdaInput);
		szerdaInput.setTextFormatter(textFormatter3);
		szerdaInput.setMaxWidth(INPUT_WIDTH);
		szerdaInput.setMinWidth(INPUT_WIDTH);

		Label csutortokLabel = new Label();
		csutortokLabel.setText("csütörtök:");

		TextField csutortokInput = new TextField();
		fields.add(csutortokInput);
		csutortokInput.setTextFormatter(textFormatter4);
		csutortokInput.setMaxWidth(INPUT_WIDTH);
		csutortokInput.setMinWidth(INPUT_WIDTH);

		Label pentekLabel = new Label();
		pentekLabel.setText("péntek:");

		TextField pentekInput = new TextField();
		fields.add(pentekInput);
		pentekInput.setTextFormatter(textFormatter5);
		pentekInput.setMaxWidth(INPUT_WIDTH);
		pentekInput.setMinWidth(INPUT_WIDTH);

		Label szombatLabel = new Label();
		szombatLabel.setText("szombat:");

		TextField szombatInput = new TextField();
		fields.add(szombatInput);
		szombatInput.setTextFormatter(textFormatter6);
		szombatInput.setMaxWidth(INPUT_WIDTH);
		szombatInput.setMinWidth(INPUT_WIDTH);

		Label vasarnapLabel = new Label();
		vasarnapLabel.setText("vasárnap:");

		TextField vasarnapInput = new TextField();
		fields.add(vasarnapInput);
		vasarnapInput.setTextFormatter(textFormatter7);
		vasarnapInput.setMaxWidth(INPUT_WIDTH);
		vasarnapInput.setMinWidth(INPUT_WIDTH);


		Button evaluateTemperatureButton = new Button("evaluate");
		evaluateTemperatureButton.setMaxWidth(INPUT_WIDTH);
		evaluateTemperatureButton.setMinWidth(INPUT_WIDTH);
		evaluateTemperatureButton.setAlignment(Pos.CENTER_LEFT);

		//BekuldesValasz
		Label responseLabel = new Label();
		responseLabel.setText("Response:");
		responseLabel.setMaxWidth(LABEL_WIDTH);
		responseLabel.setMinWidth(LABEL_WIDTH);


		TextArea responseInput = new TextArea();
		responseInput.setEditable(false);
		responseInput.maxHeight(300);
		responseInput.setWrapText(true);

		HBox responseContainer = new HBox(responseInput);
		responseContainer.setAlignment(Pos.TOP_LEFT);
		HBox.setHgrow(responseInput, Priority.ALWAYS);

		responseContainer.setPadding(new Insets(2, 0, 0, 0));
		responseContainer.setMaxHeight(200);
		responseContainer.setMaxWidth(INPUT_WIDTH * 3);
		GridPane.setColumnSpan(responseContainer, 3);

		GridPane.setConstraints(hetfoLabel, 0, 1);
		GridPane.setConstraints(hetfoInput, 0, 2);
		GridPane.setConstraints(keddLabel, 0, 3);
		GridPane.setConstraints(keddInput, 0, 4);
		GridPane.setConstraints(szerdaLabel, 0, 5);
		GridPane.setConstraints(szerdaInput, 0, 6);

		GridPane.setConstraints(csutortokLabel, 1, 1);
		GridPane.setConstraints(csutortokInput, 1, 2);
		GridPane.setConstraints(pentekLabel, 1, 3);
		GridPane.setConstraints(pentekInput, 1, 4);
		GridPane.setConstraints(szombatLabel, 1, 5);
		GridPane.setConstraints(szombatInput, 1, 6);
		GridPane.setConstraints(vasarnapLabel, 1, 7);
		GridPane.setConstraints(vasarnapInput, 1, 8);

		GridPane.setConstraints(evaluateTemperatureButton, 0, 9);
		GridPane.setConstraints(responseLabel, 0, 11);
		GridPane.setConstraints(responseContainer, 0, 12);

		grid.getChildren().addAll(hetfoInput,hetfoLabel,keddInput,keddLabel,szerdaInput,szerdaLabel,
				csutortokLabel,csutortokInput,pentekLabel,pentekInput,szombatLabel,szombatInput,vasarnapLabel,vasarnapInput,
				evaluateTemperatureButton,responseLabel,responseContainer);

		window.setScene(scene);

		evaluateTemperatureButton.setOnAction(e -> {
			List<Float> temps = new ArrayList<>();
			for(TextField field: fields){
				temps.add(Float.parseFloat(field.getText().replace(',','.')));
			}
			float result = Temperature.calculateAVG(temps);
			logger.info(String.valueOf(result));
			responseInput.setText(Temperature.howCold(result));

		});

		window.show();


	}

}
