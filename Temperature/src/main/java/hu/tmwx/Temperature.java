package hu.tmwx;

import java.util.List;

public class Temperature{
	static final String limitError = "Value of % is not in the range of permitted values.";

	public static float calculateAVG(List<Float> temps){

		float tempSum = 0;
		for(float temp : temps){
			tempSum += temp;
		}
		return tempSum/temps.size();
	}

	public static String howCold(float averageTemperature){
		if (Math.round(averageTemperature) <= 10) {
			return ("Icy cool!");
		} else if (11 <= Math.round(averageTemperature) && Math.round(averageTemperature) <= 15) {
			return ("Chilled out!");
		} else if (16 <= Math.round(averageTemperature) && Math.round(averageTemperature) <= 19) {
			return ("Cool man!");
		} else if (20 <= Math.round(averageTemperature) && Math.round(averageTemperature) <= 22) {
			return ("Too warm!");
		} else {
			return ("Hot & sweaty!");
		}
	}
}
