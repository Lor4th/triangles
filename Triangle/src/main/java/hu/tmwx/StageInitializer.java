package hu.tmwx;


import javafx.geometry.Insets;
import javafx.geometry.Pos;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import java.util.function.UnaryOperator;

@Component
public class StageInitializer implements ApplicationListener<Main.StageReadyEvent>{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	Stage window;


	private static final double WINDOW_WIDTH = 300;
	private static final double WINDOW_HEIGHT = 500;

	private static final double WINDOW_WIDTH_LEAVING = 700;
	private static final double WINDOW_HEIGHT_LEAVING = 600;

	private static final double INPUT_WIDTH = 250;
	private static final double LABEL_WIDTH = 200;


	private static final double WINDOW_WIDTH_ARRIVING = 1150;
	private static final double WINDOW_HEIGHT_ARRIVING = 650;
	private final ApplicationContext applicationContext;

	private final TextFormatter<String> textFormatter1;
	private final TextFormatter<String> textFormatter2;
	private final TextFormatter<String> textFormatter3;

	public StageInitializer( ApplicationContext applicationContext){
		this.applicationContext = applicationContext;
		UnaryOperator<TextFormatter.Change> filter = change -> {
			String text = change.getControlNewText();
			if (text.matches("(0)|([0-9]*)")) {
				return change;
			}
			return null;
		};
		TextFormatter<String> textFormatter = new TextFormatter<>(filter);
		this.textFormatter1=textFormatter;
		TextFormatter<String> textFormatter2 = new TextFormatter<>(filter);
		this.textFormatter2=textFormatter2;
		TextFormatter<String> textFormatter3 = new TextFormatter<>(filter);
		this.textFormatter3=textFormatter3;
	}


	@Override
	public void onApplicationEvent(Main.StageReadyEvent event){
		Stage primaryStage = event.getStage();

		window = primaryStage;
		window.setTitle("Triangle");

		GridPane grid = new GridPane();
		grid.setPadding(new Insets(20, 20, 20, 20));
		grid.setPrefSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		grid.setMinSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		grid.setVgap(8);
		grid.setHgap(80);

		Scene scene = new Scene(grid, WINDOW_WIDTH, WINDOW_HEIGHT);

		Label validationLabel = new Label();
		validationLabel.setText("Limit validation:");

		TextField validationInput = new TextField();
		validationInput.setEditable(false);
		if(applicationContext.getDoValidation()){
			validationInput.setText("on");
		}else{
			validationInput.setText("off");
		}
		validationInput.setMaxWidth(INPUT_WIDTH);
		validationInput.setMinWidth(INPUT_WIDTH);
		validationInput.setStyle("-fx-text-inner-color: red;");


		Label aLabel = new Label();
		aLabel.setText("a:");

		TextField aInput = new TextField();
		aInput.setPromptText("a");
		aInput.setTextFormatter(textFormatter1);
		aInput.setMaxWidth(INPUT_WIDTH);
		aInput.setMinWidth(INPUT_WIDTH);

		Label bLabel = new Label();
		bLabel.setText("b:");

		TextField bInput = new TextField();
		bInput.setPromptText("b");
		bInput.setTextFormatter(textFormatter2);
		bInput.setMaxWidth(INPUT_WIDTH);
		bInput.setMinWidth(INPUT_WIDTH);

		Label cLabel = new Label();
		cLabel.setText("c:");

		TextField cInput = new TextField();
		cInput.setPromptText("c");
		cInput.setTextFormatter(textFormatter3);
		cInput.setMaxWidth(INPUT_WIDTH);
		cInput.setMinWidth(INPUT_WIDTH);


		Button evaluateTriangleButton = new Button("evaluate");
		evaluateTriangleButton.setMaxWidth(INPUT_WIDTH);
		evaluateTriangleButton.setMinWidth(INPUT_WIDTH);
		evaluateTriangleButton.setAlignment(Pos.CENTER_LEFT);

		//BekuldesValasz
		Label responseLabel = new Label();
		responseLabel.setText("Response:");
		responseLabel.setMaxWidth(LABEL_WIDTH);
		responseLabel.setMinWidth(LABEL_WIDTH);


		TextArea responseInput = new TextArea();
		responseInput.setEditable(false);
		responseInput.maxHeight(300);
		responseInput.setWrapText(true);

		HBox responseContainer = new HBox(responseInput);
		responseContainer.setAlignment(Pos.TOP_LEFT);
		HBox.setHgrow(responseInput, Priority.ALWAYS);

		responseContainer.setPadding(new Insets(2, 0, 0, 0));
		responseContainer.setMaxHeight(200);
		responseContainer.setMaxWidth(INPUT_WIDTH * 3);
		GridPane.setColumnSpan(responseContainer, 3);

		GridPane.setConstraints(validationLabel, 0, 1);
		GridPane.setConstraints(validationInput, 0, 2);
		GridPane.setConstraints(aLabel, 0, 3);
		GridPane.setConstraints(aInput, 0, 4);
		GridPane.setConstraints(bLabel, 0, 5);
		GridPane.setConstraints(bInput, 0, 6);
		GridPane.setConstraints(cLabel, 0, 7);
		GridPane.setConstraints(cInput, 0, 8);
		GridPane.setConstraints(evaluateTriangleButton, 0, 9);
		GridPane.setConstraints(responseLabel, 0, 11);
		GridPane.setConstraints(responseContainer, 0, 12);

		grid.getChildren().addAll(validationLabel,validationInput,aInput,aLabel,bInput,bLabel,cInput,cLabel,evaluateTriangleButton,responseLabel,responseContainer);

		window.setScene(scene);

		evaluateTriangleButton.setOnAction(e -> {
			int a = Integer.parseInt(aInput.getText());
			int b = Integer.parseInt(bInput.getText());
			int c = Integer.parseInt(cInput.getText());
			String result = Triangle.validate(a,b,c,applicationContext.getDoValidation());

			responseInput.setText(result);

		});

		window.show();


	}

}
